FROM java:8

VOLUME /tmp

ADD build/libs/docker-test.jar docker-test.jar

RUN touch /docker-test.jar

EXPOSE 8780

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/docker-test.jar"]