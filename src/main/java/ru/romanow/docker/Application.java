package ru.romanow.docker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by romanow on 02.09.16
 */
@SpringBootApplication
public class Application
        extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @RestController
    public class DataController {

        @Autowired
        private DataRepository dataRepository;

        @GetMapping
        public List<Data> getData() {
            return dataRepository.findAll();
        }
    }
}