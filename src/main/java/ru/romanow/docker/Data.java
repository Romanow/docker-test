package ru.romanow.docker;

import com.google.common.base.MoreObjects;

import javax.persistence.*;

/**
 * Created by romanow on 22.05.17
 */
@Entity
@Table(name = "data")
public class Data {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String info;

    @Column
    private Integer code;

    public Integer getId() {
        return id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .omitNullValues()
                          .add("info", info)
                          .add("code", code)
                          .toString();
    }
}
