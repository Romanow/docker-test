package ru.romanow.docker;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by romanow on 22.05.17
 */
public interface DataRepository
        extends JpaRepository<Data, Integer> {}
